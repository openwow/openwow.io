FROM microsoft/dotnet:2.2-sdk
WORKDIR /app/openwow.io/src/OpenWoW.Web

# Required inside Docker, otherwise file-change events may not trigger
ENV DOTNET_USE_POLLING_FILE_WATCHER 1

# This will build and launch the server in a loop, restarting whenever a *.cs file changes
ENTRYPOINT dotnet watch run
