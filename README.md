# openwow.io 

OpenWoW website. Through the magical power of Docker Compose, this will download and run:

* a local PostgreSQL server
* a local Redis server
* an ASP.NET Core app with automatic reloading

## Requirements

* [.NET Core 2.2 SDK](https://dotnet.microsoft.com/download/dotnet-core/2.2)
* Visual Studio 2017 or any useful text editor (eg Visual Studio Code)
* Docker:
    - [Docker for Windows](https://docs.docker.com/docker-for-windows/) - Docker Desktop needs to be running _and logged in_, downloading images gives weird errors otherwise
    - Linux: ???
    - OS X: ???

If you want to be able to test multiple WoW branch behavior, you'll need to add the following to your `hosts` file:

`127.0.0.1	openwow.test beta.openwow.test classic.openwow.test ptr.openwow.test api.openwow.test`

## Usage

1. Start a Command Prompt/Powershell/shell.
1. Run **`docker-compose up`** in the base directory, this will take a while the first time.
1. Navigate to **`http://localhost:50000`** (or **`http://openwow.test:50000`** if you added the `hosts` entry).
1. Start editing the solution, it will automatically rebuild/restart the service once you save your changes.
