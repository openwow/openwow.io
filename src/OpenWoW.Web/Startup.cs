﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Database.Web.Extensions;
using OpenWoW.Database.Web.Repositories;
using OpenWoW.Web.Controllers;
using StackExchange.Redis;
using WebMarkupMin.AspNetCore2;

namespace OpenWoW.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddResponseCompression();
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddApplicationPart(typeof(HomeController).Assembly);

            /*services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });*/

            services.AddWebMarkupMin(options =>
            {
                options.AllowMinificationInDevelopmentEnvironment = true;
                options.AllowCompressionInDevelopmentEnvironment = true;
            })
                .AddHtmlMinification();

            services.AddScoped<IViewRenderService, ViewRenderService>();

            ConfigureDatabase(services);
        }

        protected virtual void ConfigureDatabase(IServiceCollection services)
        {
            services.ConfigurePostgres(Configuration);

            // Redis
            var redis = ConnectionMultiplexer.Connect(Configuration.GetConnectionString("Redis"));
            services.AddDataProtection()
                .PersistKeysToStackExchangeRedis(redis, "DataProtectionKeys");

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Configuration.GetConnectionString("Redis");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IWebRepository webRepository)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                //app.UseHsts(); // Don't use HSTS here, it's set via Cloudflare
            }

            //app.UseHttpsRedirection(); // Set via cloudflare
            app.UseResponseCompression();
            app.UseStaticFiles();
            //app.UseCookiePolicy(); // We have no cookies right now

            app.UseWebMarkupMin();

            app.UseSubSites();

            app.UseMvc();

            // Initialize all repositories and wait for them to be ready
            Task.WaitAll(new Task[]
            {
                webRepository.Initialize(),
            });
        }
    }
}
