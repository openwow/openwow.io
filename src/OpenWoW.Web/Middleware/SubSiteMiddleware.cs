﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using OpenWoW.Database.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.Web
{
    public class SubSiteMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public SubSiteMiddleware(RequestDelegate next, ILogger<SubSiteMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext httpContext, IWebRepository repository)
        {
            var sw = Stopwatch.StartNew();

            var currentHost = httpContext.Request.Host.Host;
            if (currentHost.EndsWith(".io"))
            {
                currentHost = currentHost.Substring(0, currentHost.Length - 2) + "$";
            }
            else if (currentHost.EndsWith(".test"))
            {
                currentHost = currentHost.Substring(0, currentHost.Length - 4) + "$";
            }

            var subSites = await repository.GetSubSites();
            SiteContext.ActiveSite = subSites.Where(s => s.Hostname == currentHost).FirstOrDefault() ?? subSites.First();

            sw.Stop();
            _logger.LogDebug("Invoke took {0}ms", sw.ElapsedMilliseconds);

            // Call the next middleware
            await _next(httpContext);
        }
    }
}
