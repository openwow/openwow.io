﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.Web
{
    public static class IApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseSubSites(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SubSiteMiddleware>();
        }
    }
}
