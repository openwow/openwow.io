﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.Web
{
    public static class HtmlUtilities
    {
        public static List<string> BuildTooltipRow(string first, string second)
        {
            if (!string.IsNullOrWhiteSpace(first) || !string.IsNullOrWhiteSpace(second))
            {
                var ret = new List<string>();
                if (!string.IsNullOrWhiteSpace(first))
                {
                    ret.Add(first);
                }
                if (!string.IsNullOrWhiteSpace(second))
                {
                    ret.Add(second);
                }
                return ret;
            }
            else
            {
                return null;
            }
        }
    }
}
