﻿using OpenWoW.Common;
using System.Collections.Generic;

namespace OpenWoW.Web
{
    public class TooltipSpell : ITooltipSpell
    {
        public int SpellID { get; set; }
        public string Name { get; set; }
        public string ParsedDescription { get; set; }

        public string CastTimeText { get; set; }
        public string CooldownText { get; set; }
        public string GlobalCooldownText { get; set; }
        public string PowerText { get; set; }
        public string ProcsPerMinuteText { get; set; }
        public string RangeText { get; set; }
        public string RankText { get; set; }

        /*
            Spell details seems to consist of two left-aligned rows of two columns.If a row is empty it isn't displayed at all.

            [resource cost]       [range]
            [cast time]        [cooldown]
        */
        public List<List<string>> DetailsRows
        {
            get
            {
                return new List<List<string>>
                {
                    HtmlUtilities.BuildTooltipRow(PowerText, RangeText),
                    HtmlUtilities.BuildTooltipRow(CastTimeText, CooldownText),
                };
            }
        }
    }
}
