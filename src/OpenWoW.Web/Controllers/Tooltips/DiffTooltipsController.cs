﻿using EnumsNET;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OpenWoW.Common;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Database.Web.Repositories;
using OpenWoW.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.Web.Controllers
{
    [Route("diff-tooltips")]
    public class DiffTooltipsController : Controller
    {
        private readonly IViewRenderService _viewRenderService;
        private readonly IWebRepository _webRepository;

        public DiffTooltipsController(IViewRenderService viewRenderService, IWebRepository webRepository)
        {
            _viewRenderService = viewRenderService;
            _webRepository = webRepository;
        }

        [HttpGet("{entityType:int}-{entityId:int}-{oldBuildId:int}-{newBuildId:int}")]
        public async Task<IActionResult> GetTooltips(int entityType, int entityId, int oldBuildId, int newBuildId)
        {
            var actualEntityType = (EntityType)entityType;
            if (!actualEntityType.IsValid())
            {
                return NotFound();
            }

            return await RenderDiffTooltip(actualEntityType, entityId, oldBuildId, newBuildId);
        }

        private async Task<IActionResult> RenderDiffTooltip(EntityType entityType, int entityId, int oldBuildId, int newBuildId)
        {
            var oldDiffTooltip = await _webRepository.GetDiffTooltip(oldBuildId, entityType, entityId);
            var newDiffTooltip = await _webRepository.GetDiffTooltip(newBuildId, entityType, entityId);
            if (oldDiffTooltip == null && newDiffTooltip == null)
            {
                return NotFound();
            }

            var dtvm = new DiffTooltipViewModel();

            if (oldDiffTooltip != null)
            {
                dtvm.Icon = oldDiffTooltip.Icon;
                dtvm.OldBuild = oldDiffTooltip.Build;
                dtvm.OldTooltip = await RenderTooltip(entityType, oldDiffTooltip);
            }

            if (newDiffTooltip != null)
            {
                dtvm.Icon = newDiffTooltip.Icon ?? dtvm.Icon;
                dtvm.NewBuild = newDiffTooltip.Build;
                dtvm.NewTooltip = await RenderTooltip(entityType, newDiffTooltip);
            }

            // AJAX requests only want a rendered multi-tooltip, visiting the URL directly should show with layout
            if (HttpContext.Request.IsAjaxRequest())
            {
                return PartialView("MultiTooltip", dtvm);
            }
            else
            {
                return View("MultiTooltip", dtvm);
            }
        }

        private async Task<string> RenderTooltip(EntityType entityType, DiffTooltip diffTooltip)
        {
            switch (entityType)
            {
                case EntityType.Achievement:
                    var achievement = JsonConvert.DeserializeObject<ITooltipAchievement>(diffTooltip.JsonData);
                    return await _viewRenderService.RenderToStringAsync("Shared/_AchievementBody", achievement);

                case EntityType.Item:
                    var item = JsonConvert.DeserializeObject<ITooltipItem>(diffTooltip.JsonData);
                    return await _viewRenderService.RenderToStringAsync("Shared/_ItemBody", item);

                case EntityType.Spell:
                    var spell = JsonConvert.DeserializeObject<TooltipSpell>(diffTooltip.JsonData);
                    return await _viewRenderService.RenderToStringAsync("Tooltips/_SpellBody", spell);

                default:
                    throw new NotImplementedException($"RenderTooltip doesn't know how to render a {entityType.ToString()}");
            }
        }
    }
}
