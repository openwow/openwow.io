﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using OpenWoW.Database.Web.Entities;

namespace OpenWoW.Web.Models
{
    public class DiffTooltipViewModel
    {
        public string Icon { get; set; }
        public BuildData OldBuild { get; set; }
        public BuildData NewBuild { get; set; }
        public string OldTooltip { get; set; }
        public string NewTooltip { get; set; }
    }
}
