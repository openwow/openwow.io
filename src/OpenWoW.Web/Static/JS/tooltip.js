﻿var Diff = require('diff');

window.OpenWoW = window.OpenWoW || {};
window.OpenWoW.tooltip = (function () {
    var diffMultiTooltip = function () {
        let multiTooltip = document.getElementById('multi-tooltip');
        if (multiTooltip === null) return;

        let tooltips = multiTooltip.querySelectorAll('.tooltip > p');
        if (tooltips.length !== 2) return;

        let diffResults = Diff.diffWords(tooltips[0].innerHTML, tooltips[1].innerHTML);
        console.log(diffResults);

        let oldHtml = '',
            newHtml = '',
            oldSpan = false,
            newSpan = false;
        for (let i = 0; i < diffResults.length; i++) {
            var part = diffResults[i];
            if (part.removed === true) {
                if (!oldSpan) {
                    oldSpan = true;
                    oldHtml += '<span class="diff-removed">';
                }
                oldHtml += part.value;
            }
            else if (part.added === true) {
                if (!newSpan) {
                    newSpan = true;
                    newHtml += '<span class="diff-added">';
                }
                newHtml += part.value;
            }
            else if (part.value === " ") {
                oldHtml += part.value;
                newHtml += part.value;
            }
            else {
                if (oldSpan) {
                    oldHtml += '</span>';
                    oldSpan = false;
                }
                if (newSpan) {
                    newHtml += '</span>';
                    newSpan = false;
                }
                oldHtml += part.value;
                newHtml += part.value;
            }
        }

        if (oldSpan) {
            oldHtml += '</span>';
        }
        if (newSpan) {
            newHtml += '</span>';
        }

        tooltips[0].innerHTML = oldHtml;
        tooltips[1].innerHTML = newHtml;
    };
    
    return {
        diffMultiTooltip,
    };
})();
