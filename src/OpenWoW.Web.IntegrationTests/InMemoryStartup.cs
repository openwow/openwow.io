﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Repositories;
using OpenWoW.Web.IntegrationTests.Repositories;

namespace OpenWoW.Web.IntegrationTests
{
    public class InMemoryStartup : Startup
    {
        public InMemoryStartup(IConfiguration configuration) : base(configuration)
        { }

        protected override void ConfigureDatabase(IServiceCollection services)
        {
            services.AddDbContext<WebDbContext>(options =>
            {
                options.UseInMemoryDatabase("hello I am a test database");
            });

            services.AddTransient<IWebRepository, TestWebRepository>();
        }
    }
}
