﻿using Microsoft.Extensions.Caching.Distributed;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Database.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.Web.IntegrationTests.Repositories
{
    public class TestWebRepository : WebRepository
    {
        public TestWebRepository(IDistributedCache cache, WebDbContext context) : base(cache, context)
        { }

        public override async Task Initialize()
        {
            // The in-memory database provider can't deal with migrations, just create the database	
            await _context.Database.EnsureCreatedAsync();
        }
    }
}
