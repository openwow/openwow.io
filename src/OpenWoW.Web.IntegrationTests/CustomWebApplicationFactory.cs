﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using System.IO;

namespace OpenWoW.Web.IntegrationTests
{
    public class CustomWebApplicationFactory : WebApplicationFactory<InMemoryStartup>
    {
        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            return WebHost.CreateDefaultBuilder()
                .UseStartup<InMemoryStartup>();
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            // ASP.NET Core can't find our content root because it's in "src\Blah" instead of "Blah". Cool.
            // - https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-2.2#how-the-test-infrastructure-infers-the-app-content-root-path
            builder.UseSolutionRelativeContentRoot(Path.Combine("src", "OpenWoW.Web"));
            base.ConfigureWebHost(builder);
        }
    }
}
