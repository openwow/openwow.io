FROM mcr.microsoft.com/dotnet/core/sdk:2.2-bionic AS build

RUN apt-get update && apt-get install -y build-essential nodejs npm

WORKDIR /app
RUN git clone https://gitlab.com/openwow/common.git/

# Restore npm packages now so that this step is cached
COPY src/OpenWoW.Web/package*.json /app/openwow.io/src/OpenWoW.Web/
WORKDIR /app/openwow.io/src/OpenWoW.Web
RUN npm install

WORKDIR /app/openwow.io
COPY . .
#RUN dotnet test

WORKDIR /app/openwow.io/src/OpenWoW.Web
RUN npm run build-prod
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine AS runtime
WORKDIR /app/openwow.io
COPY --from=build /app/openwow.io/src/OpenWoW.Web/out ./

ENV ASPNETCORE_URLS "http://0:5000"
EXPOSE 5000
ENTRYPOINT ["dotnet", "OpenWoW.Web.dll"]
